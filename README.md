# Plotter 

Image processing and drawing animation resembling plotters of the 1960s.

![](piece_sets/futurism/animation.gif)

Includes tuned edge detection algorithms for various styles of art along with scripts to animate the drawing of the edges to resemble the plotting process.  
Frames are saved incrementally throughout plotting and then can be stitched together into gifs.  
Also includes tooling to help in the discovery of proper tuning parameters for new pieces and styles.

Some precomputed animates are saved in the `piece_sets` directory.  
The original pieces they are based upon are in the `original_pieces` directory.
