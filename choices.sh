#!/bin/bash

# ./gif.sh abstractexpressionism abstract
./gif.sh artnouveau artnouveau
./gif.sh baroque rococo
./gif.sh classicism futurism
./gif.sh constructivism abstract
./gif.sh cubism futurism
./gif.sh dada futurism
./gif.sh expressionism futurism
./gif.sh fauvism artnouveau
./gif.sh futurism futurism
./gif.sh impressionism rococo
./gif.sh minimalism suprematism
./gif.sh neoclassicism suprematism
./gif.sh neoimpressionism abstract
./gif.sh pointillism abstract
./gif.sh popart futurism
./gif.sh postimpressionism suprematism
./gif.sh rococo rococo
./gif.sh suprematism artnouveau
./gif.sh surrealism futurism
