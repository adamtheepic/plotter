#!/bin/bash

for piece in `ls original_pieces | awk -F. '{print $1}'`; do
	for style in `find . -type d -name target*`; do 
		feh $style/$piece/`ls -v $style/$piece | tail -n 1` &
	done
	ic original_pieces/$piece.jpg &
	read await
	pkill feh
done

