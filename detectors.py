import cv2
import numpy as np

# futurism
def futurism_detect_edges(image):

    for _ in range(2):
        image = cv2.bilateralFilter(image, 4, 75, 75)

    # sigma = 6
    # intensity = np.median(image)
    # lower = int(max(0, (1.0 - sigma) * intensity))
    # upper = int(max(255, (1.0 + sigma) * intensity))

    lower, _ = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    upper = lower*2

    upper *= .2
    lower *= .2

    edges = cv2.Canny(image,lower,upper)
    minLineLength = 30
    maxLineGap = 95
    lines = cv2.HoughLinesP(edges, cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength, maxLineGap)
    if not lines is None:
        for x in range(len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                pts = np.array([[x1,y1], [x2,y2]], np.int32)
                cv2.polylines(edges, [pts], True, (0,255,0))
    return edges

# suprematism
def suprematism_detect_edges(image):

    for _ in range(2):
        image = cv2.bilateralFilter(image, 4, 75, 75)

    # sigma = 6
    # intensity = np.median(image)
    # lower = int(max(0, (1.0 - sigma) * intensity))
    # upper = int(max(255, (1.0 + sigma) * intensity))

    lower, _ = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    upper = lower*2

    upper *= .1
    lower *= .1

    edges = cv2.Canny(image,lower,upper)
    minLineLength = 30
    maxLineGap = 95
    lines = cv2.HoughLinesP(edges, cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength, maxLineGap)
    if not lines is None:
        for x in range(len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                pts = np.array([[x1,y1], [x2,y2]], np.int32)
                cv2.polylines(edges, [pts], True, (0,255,0))
    return edges

# rococo
def rococo_detect_edges(image):

    for _ in range(0):
        image = cv2.bilateralFilter(image, 4, 75, 75)

    # sigma = 6
    # intensity = np.median(image)
    # lower = int(max(0, (1.0 - sigma) * intensity))
    # upper = int(max(255, (1.0 + sigma) * intensity))

    lower, _ = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    upper = lower*2

    upper *= 1
    lower *= 1

    edges = cv2.Canny(image,lower,upper)
    minLineLength = 30
    maxLineGap = 95
    lines = cv2.HoughLinesP(edges, cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength, maxLineGap)
    if not lines is None:
        for x in range(len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                pts = np.array([[x1,y1], [x2,y2]], np.int32)
                cv2.polylines(edges, [pts], True, (0,255,0))
    return edges

# abstract expressionism
def abstract_detect_edges(image):

    for _ in range(6):
        image = cv2.bilateralFilter(image, 4, 75, 75)

    # sigma = 6
    # intensity = np.median(image)
    # lower = int(max(0, (1.0 - sigma) * intensity))
    # upper = int(max(255, (1.0 + sigma) * intensity))

    lower, _ = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    upper = lower*2

    upper *= 0.1
    lower *= 0.1

    edges = cv2.Canny(image,lower,upper)
    minLineLength = 30
    maxLineGap = 95
    lines = cv2.HoughLinesP(edges, cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength, maxLineGap)
    if not lines is None:
        for x in range(len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                pts = np.array([[x1,y1], [x2,y2]], np.int32)
                cv2.polylines(edges, [pts], True, (0,255,0))
    return edges

# art nouveau
def artnouveau_detect_edges(image):

    for _ in range(5):
        image = cv2.bilateralFilter(image, 4, 75, 75)

    # sigma = 6
    # intensity = np.median(image)
    # lower = int(max(0, (1.0 - sigma) * intensity))
    # upper = int(max(255, (1.0 + sigma) * intensity))

    lower, _ = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    upper = lower*2

    upper *= 0.3
    lower *= 0.3

    edges = cv2.Canny(image,lower,upper)
    minLineLength = 30
    maxLineGap = 95
    lines = cv2.HoughLinesP(edges, cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength, maxLineGap)
    if not lines is None:
        for x in range(len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                pts = np.array([[x1,y1], [x2,y2]], np.int32)
                cv2.polylines(edges, [pts], True, (0,255,0))
    return edges


edge_detectors = {"futurism": futurism_detect_edges, "suprematism": suprematism_detect_edges, "rococo": rococo_detect_edges, "abstract": abstract_detect_edges, "artnouveau": artnouveau_detect_edges}
