#!/bin/bash

[[ $# -eq 3 ]] && delay=$3 || delay=0

path="target-$2/$1"
images=`find $path -type f | grep -Eo '[0-9]*' | sort -n | awk '{print "'$path/$1'" $0 "'.png'"}'`

convert -coalesce -layers Optimize -strip -colors 2 -delay $delay -loop 0 $images $1.gif

#convert -delay 5 fast-input.gif slow-output.gif # <- slow down after creation
