import os
import cv2
import numpy as np
import pygame
import pygame.locals
import math
from detectors import edge_detectors

TITLE = "Plotter"
DIR = "original_pieces/"


line_lengths = []
frame = 0

drawn = set()

def draw_point(surface, coordinates, image_name, target):
    if coordinates:
        surface.set_at(coordinates[::-1], pygame.Color('white'))
        pygame.display.update()
        global frame
        if not frame%10:
            pygame.image.save(surface, 'target-'+target+'/'+image_name+'/'+image_name+str(frame)+'.png')
        frame+=1

def get_neighborhood(image, coordinates):
    potential_neighbors = [coordinates]
    neighbors = []
    global drawn
    while potential_neighbors:
        coordinates = potential_neighbors.pop()
        try:
            if coordinates in drawn or not image.item(coordinates):
                continue
        except:
            continue
        drawn.add(coordinates)
        neighbors.append(coordinates)
        x, y = coordinates
        for i in range(-1,2):
            for j in range(-1,2):
                potential_neighbors.append((x+i,y+j))
    if len(neighbors) > 0:
        global line_lengths
        line_lengths.append(len(neighbors))
    return neighbors

def draw_image(image, image_name, target):
    pygame.init()
    global frame
    global drawn
    frame = 0 
    drawn = set()


    width, height = image.shape
    surface = pygame.display.set_mode((width, height), pygame.locals.DOUBLEBUF | pygame.locals.SRCALPHA)
    pygame.display.set_caption(TITLE)

    for x in range(width):
        for y in range(height):
            list(map(lambda coordinates: draw_point(surface, coordinates, image_name, target), get_neighborhood(image, (x,y))))

def main():
    for filename in os.listdir(DIR):
        for target, detector in edge_detectors.items():
            print(filename, target)
            image_name=filename.split('.')[0]
            try:
                os.mkdir("target-"+target)
            except:
                pass
            try:
                os.mkdir("target-"+target+'/'+image_name)
            except:
                continue
            draw_image(detector(cv2.imread(DIR+filename,0)), image_name, target)

            global line_lengths
            
            median = np.median(line_lengths)
            sum_ = sum(line_lengths)
            fitness = math.log((median**2)*(sum_**5))

            print("Median:", median)
            print("Sum:", sum_)
            print("Fitness:", fitness)

            line_lengths = []
    print("Done")

if __name__ == '__main__':
    main()
